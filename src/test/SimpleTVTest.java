package test;

import org.gstreamer.Element;
import org.gstreamer.ElementFactory;
import org.gstreamer.Gst;
import org.gstreamer.Pipeline;
import org.gstreamer.State;
import org.gstreamer.interfaces.Tuner;
import org.gstreamer.interfaces.TunerChannel;

public class SimpleTVTest {
	public static void main(String args[]) {
		Gst.init();
		final Pipeline pipe = new Pipeline("main pipe");
		Element src = ElementFactory.make("v4l2src", "source");
		Element sink = ElementFactory.make("xvimagesink", "sink");
		
		src.set("device", "/dev/video2");
		
		pipe.addMany(src, sink);
		Element.linkMany(src, sink);
		
		pipe.setState(State.PLAYING);
		
		Tuner tuner = Tuner.wrap(src);
		TunerChannel chan = tuner.getChannel();
		System.out.println("Tuner channel is tuning: "+chan.isTuningChannel());
		System.out.println("Tuner channel freq: "+chan.getFrequency());
		
	
		Gst.main();
	}

}
