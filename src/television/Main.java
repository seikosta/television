package television;

import java.io.IOException;
import java.util.logging.Logger;

import org.gstreamer.Gst;

import television.TVDataHandler.CannotChangeFrequencyException;
import television.TVDataHandler.GstreamerException;
import television.channel.Channel;
import television.device.DeviceDiscover;
import television.gui.GUI;

public class Main {
	public static void main(String[] args) {
		Configuration conf = Configuration.getInstance();
		try {
			conf.read();
		} catch (IOException e) {
			log.severe("Error occured during reading the config file: "+e.getMessage());
			e.printStackTrace();
		}

		args = Gst.init("Television", args);
		DeviceDiscover.discoverDevices();
		
		TVDataHandler tdh = new TVDataHandler();
		try {
			tdh.constructMainPipeline(
					DeviceDiscover.searchAudioDeviceForString(conf.getProperty("AudioInputDevice")),
					DeviceDiscover.searchAudioDeviceForString(conf.getProperty("AudioOutputDevice")),
					DeviceDiscover.searchVideoDeviceForString(conf.getProperty("VideoInputDevice"))					
			);
			String chanName = conf.getProperty("DefaultSelection");
			if (chanName != null) {
				Channel chan = Channel.valueOf(chanName);
				if (chan != null)
					tdh.tuneChannel(chan);
			}
		} catch (GstreamerException e) {
			log.severe("Gstreamer exception in main pipeline construction: "+e.getMessage());
			e.printStackTrace();
		} catch (CannotChangeFrequencyException e) {
			log.severe("Gstreamer exception in default channel selection: "+e.getMessage());			
			e.printStackTrace();
		}
		(new GUI(tdh)).displayGUI();
		tdh.destructMainPipeline();
		
		try {
			conf.store();
		} catch (IOException e) {
			log.severe("Error occured during writing the config file: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(Main.class.getName());

}
