package television;

import java.util.logging.Logger;

import org.gstreamer.Bin;
import org.gstreamer.Caps;
import org.gstreamer.Element;
import org.gstreamer.ElementFactory;
import org.gstreamer.GhostPad;
import org.gstreamer.Pad;
import org.gstreamer.Pipeline;
import org.gstreamer.State;
import org.gstreamer.event.EOSEvent;
import org.gstreamer.interfaces.Tuner;
import org.gstreamer.interfaces.TunerChannel;

import television.channel.Channel;
import television.device.AudioDevice;
import television.device.VideoDevice;
import television.record.Format;
import television.record.Resolution;
import television.rtsp.RTSPServer;

public class TVDataHandler {
	private TunerChannel tc;
	private static int sequence = 0; //Used to uniquely naming gstreamer elements
	
	private Pipeline pipe;
	
	private AudioDevice aInputDevice;
	private AudioDevice aOutputDevice;
	private VideoDevice vInputDevice;
	
	private Channel tunedChannel = null;
	
	private Bin aBin;
	private Element aSrc = null;
	private Element aCaps;
	private Element aTee;
	private Element aFakeSinkQueue;
	private Element aFakeSink;
	private Element aSinkQueue;
	private Element aRecordQueue;
	private Element aSink = null;
	private Element aRTPQueue;
	
	private Bin vBin;
	private Element vSrc = null;
	private Element vTee;
	private Element vFakeSinkQueue;
	private Element vFakeSink;
	private Element vSinkQueue;
	private Element vRecordQueue;
	private boolean tunerCapable = false;
	private Element vRTPQueue;
	
	//Record settings
	private boolean recordingInProgress = false;
	private Format recordingFormat;
	private Resolution recordingResolution;
	private String recordingPath;
	
	//Record elements
    private Element vRecordScale;
    private Element vRecordScaleCaps;
    private Element vRecordCsp;
    private Element vRecordEnc;
    private Element aRecordEnc;
    private Element aRecordConvert;
    private Element recordMux;
    private Element fRecordSink;
    
    //RTSP stuff
    private RTSPServer rtspServer = null;
	Element vRTPvideoscale;
	Element vRTPcapsfilter;
	Element vRTPcsp;
	Element vRTPencoder;
	Element vRTPpay;
	Element vRTPrtpbin;
	Element vRTPrtpUdpSink;
	Element vRTPrtcpUdpSink;
	Element vRTPrtcpUdpSrc;


	private Long changeToHz = new Long(0);
		
	public long getFreqency() {
		if (!tunerCapable)
			return -1;
		return tc.getFrequency(); 
	}
	
	public void setFreqency(long freq) throws CannotChangeFrequencyException {
		if (!tunerCapable) {
			throw new CannotChangeFrequencyException();
		}

		changeToHz = freq;

		new Thread( new Runnable() {
			public void run() {
				synchronized(changeToHz) {
					while(changeToHz != 0L) {
						tc.setFrequency(changeToHz);
						changeToHz = 0L;
					}
				}
			}
		}).start();
	}
	
	public void tuneChannel(Channel ch) throws CannotChangeFrequencyException {
		tunedChannel = ch;
		setFreqency(ch.getFreqHz());
	}
	
	public Channel getTunedChannel() {
		return tunedChannel;
	}
	
	public int getSignalStrength() {
		if (!tunerCapable)
			return -1;
		return tc.getSignalStrength(); 
	}
	
	public RTSPServer getRtspServer() {
		//------------ RTSP --------------//
		return rtspServer;
	}

	public void setRtspServer(RTSPServer rtspServer) {
		this.rtspServer = rtspServer;
	}

	public void startRTP(String host, int rtpPort, int rtcpInPort, int rtcpOutPort) throws GstreamerException {
		vRTPvideoscale = ElementFactory.make("videoscale", "videoscale"+nextID());
		vRTPcapsfilter = ElementFactory.make("capsfilter", "capsfilter"+nextID());
		vRTPcapsfilter.setCaps(new Caps("video/x-raw-yuv, width=160, height=120"));
		vRTPcsp = ElementFactory.make("ffmpegcolorspace", "csp"+nextID());
		vRTPencoder = ElementFactory.make("x264enc", "x264enc"+nextID());
		vRTPencoder.set("bitrate", 512);
		vRTPpay = ElementFactory.make("rtph264pay", "rtph264pay"+nextID());
		vRTPrtpbin = ElementFactory.make("gstrtpsession", "gstrtpbin"+nextID());
		vRTPrtpUdpSink = ElementFactory.make("udpsink", "udpsink"+nextID());
		vRTPrtpUdpSink.set("port", rtpPort);
		vRTPrtpUdpSink.set("host", host);
		vRTPrtpUdpSink.set("async", false);
		vRTPrtcpUdpSink = ElementFactory.make("udpsink", "udpsink"+nextID());
		vRTPrtcpUdpSink.set("port", rtcpOutPort);
		vRTPrtcpUdpSink.set("host", host);
		vRTPrtcpUdpSink.set("sync", false);
		vRTPrtcpUdpSink.set("async", false);
		vRTPrtcpUdpSrc = ElementFactory.make("udpsrc", "udpsrc"+nextID());
		vRTPrtcpUdpSrc.set("port", rtcpInPort);
		
		pipe.addMany(vRTPvideoscale, vRTPcapsfilter, vRTPcsp, vRTPencoder, vRTPpay,
				vRTPrtpbin, vRTPrtpUdpSink, vRTPrtcpUdpSink, vRTPrtcpUdpSrc);
		
		
		for (Pad p: vBin.getSrcPads()) {
			if (p.getName().equals("rtp_src")) {
				p.link(vRTPvideoscale.getSinkPads().get(0));
				break;
			}
		}
		vBin.getSrcPads().get(0).link(vRTPvideoscale.getSinkPads().get(0));
		
		Element.linkMany(vRTPvideoscale, vRTPcapsfilter, vRTPcsp, vRTPencoder, vRTPpay);
		vRTPpay.getSrcPads().get(0).link(vRTPrtpbin.getRequestPad("send_rtp_sink"));
		vRTPrtpbin.getStaticPad("send_rtp_src").link(vRTPrtpUdpSink.getSinkPads().get(0));
		vRTPrtpbin.getRequestPad("send_rtcp_src").link(vRTPrtcpUdpSink.getSinkPads().get(0));
		vRTPrtcpUdpSrc.getSrcPads().get(0).link(vRTPrtpbin.getRequestPad("recv_rtcp_sink"));
		
		vRTPvideoscale.setState(State.PLAYING);
		vRTPcapsfilter.setState(State.PLAYING);
		vRTPcsp.setState(State.PLAYING);
		vRTPencoder.setState(State.PLAYING);
		vRTPpay.setState(State.PLAYING);
		vRTPrtpbin.setState(State.PLAYING);
		vRTPrtpUdpSink.setState(State.PLAYING);
		vRTPrtcpUdpSink.setState(State.PLAYING);
		vRTPrtcpUdpSrc.setState(State.PLAYING);
		
		//Video linkin'
		Pad vTeePad = vTee.getRequestPad("src%d");
		if (vTeePad == null) {
			throw new GstreamerException("Cannot request a pad from tee");
		}
		if (!vTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		vTeePad.link(vRTPQueue.getSinkPads().get(0));
		if (!vTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");
		}
	}
	
	public void stopRTP() throws GstreamerException {
		Pad vTeePad = vRTPQueue.getSinkPads().get(0).getPeer();
		if (vTeePad == null) {
			throw new GstreamerException("Cannot get vTeepad");			
		}
		if (!vTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		if (!vTeePad.unlink(vRTPQueue.getSinkPads().get(0))) {
			throw new GstreamerException("vSinkQueue cannot be unlinked from vTee");
		}
		if (!vTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");	
		}
		vTee.releaseRequestPad(vTeePad);
		
		Pad binPad = vRTPvideoscale.getSinkPads().get(0).getPeer();
		binPad.unlink(vRTPvideoscale.getSinkPads().get(0));
		
		Element.unlinkMany(vRTPvideoscale, vRTPcapsfilter,vRTPcsp,  vRTPencoder,vRTPpay, vRTPrtpbin);
		Element.unlinkMany(vRTPrtpbin, vRTPrtpUdpSink);
		Element.unlinkMany(vRTPrtpbin, vRTPrtcpUdpSink);
		Element.unlinkMany(vRTPrtcpUdpSrc, vRTPrtpbin);
		
		vRTPvideoscale.setState(State.NULL);
		vRTPcapsfilter.setState(State.NULL);
		vRTPcsp.setState(State.NULL);
		vRTPencoder.setState(State.NULL);
		vRTPpay.setState(State.NULL);
		vRTPrtpbin.setState(State.NULL);
		vRTPrtpUdpSink.setState(State.NULL);
		vRTPrtcpUdpSink.setState(State.NULL);
		vRTPrtcpUdpSrc.setState(State.NULL);
	}


	public void unsetVideoSink(Element vSink) throws GstreamerException {
		//Pad vTeePad = vSinkQueue.getStaticPad("sink").getPeer();
		Pad vTeePad = vSinkQueue.getSinkPads().get(0).getPeer();
		if (vTeePad == null) {
			throw new GstreamerException("Cannot get vTeepad");			
		}
		if (!vTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		if (!vTeePad.unlink(vSinkQueue.getSinkPads().get(0))) {
			throw new GstreamerException("vSinkQueue cannot be unlinked from vTee");
		}
		if (!vTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");	
		}
		vTee.releaseRequestPad(vTeePad);
		Element.unlinkMany(vSinkQueue, vSink);
		vBin.removeMany(vSinkQueue, vSink);

		vSinkQueue.setState(org.gstreamer.State.NULL);
		vSink.setState(org.gstreamer.State.NULL);
		vSinkQueue.dispose();
		//vSink.dispose();
		vSinkQueue = null;
		//vSink = null;
		//pipe.setState(org.gstreamer.State.PAUSED);
	}
	
	public void setVideoSink(Element vSink) throws GstreamerException {
		//pipe.setState(org.gstreamer.State.PLAYING);
		vSinkQueue = ElementFactory.make("queue", "vSinkQueue"+nextID());
		vBin.addMany(vSinkQueue, vSink);
		if (!Element.linkMany(vSinkQueue, vSink)) {
			throw new GstreamerException("vSinkQueue and vSink cannot be linked");			
		}
		Pad vTeePad = vTee.getRequestPad("src%d");
		if (vTeePad == null) {
			throw new GstreamerException("Cannot request a pad from tee");
		}
		if (!vTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		vTeePad.link(vSinkQueue.getSinkPads().get(0));
		vSinkQueue.setState(org.gstreamer.State.PAUSED);
		vSink.setState(org.gstreamer.State.PAUSED);
		if (!vTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");
		}
	}
	
	public void startRecording(Format format, Resolution resolution, String filePath) throws GstreamerException {
		recordingInProgress = true;
		recordingFormat = format;
		recordingResolution = resolution;
		recordingPath = filePath;
		
		log.info("Creating video encoder: "+format.getVideo().getEncoder());
		log.info("Creating audio encoder: "+format.getAudio().getEncoder());
		log.info("Creating container: "+format.getContainer().getMuxer());
		
        vRecordScale= ElementFactory.make("videoscale", "videoscale"+nextID());
        vRecordScaleCaps = ElementFactory.make("capsfilter", "capsfilter"+nextID());
        vRecordScaleCaps.setCaps(new Caps("video/x-raw-yuv, width="+resolution.getWidth()+", height="+resolution.getHeight()));
        vRecordCsp = ElementFactory.make("ffmpegcolorspace", "recordCSP"+nextID());
        vRecordEnc  = ElementFactory.make(format.getVideo().getEncoder(), "recordvideoencoder"+nextID());
        aRecordEnc  = ElementFactory.make(format.getAudio().getEncoder(), "recordaudioencoder"+nextID());
        aRecordConvert = ElementFactory.make("audioconvert", "aconv"+nextID());
        recordMux   = ElementFactory.make(format.getContainer().getMuxer(), "recordmuxer"+nextID());
        fRecordSink = ElementFactory.make("filesink", "recordingfilesink"+nextID());
        fRecordSink.set("location", filePath);
        
        pipe.addMany(vRecordScale, vRecordScaleCaps, vRecordCsp, vRecordEnc, aRecordConvert, aRecordEnc, recordMux, fRecordSink);
        log.warning("vBin->vRecordScale: " + vBin.getPads().get(0).link(vRecordScale.getSinkPads().get(0)));
        log.warning("vRecordScale->vRecordScaleCaps->vRecordCsp->vRecordEnc->recordMux: "
        		+Element.linkMany(vRecordScale, vRecordScaleCaps, vRecordCsp, vRecordEnc, recordMux));
        log.warning("aBin->aRecordConv: " + aBin.getPads().get(0).link(aRecordConvert.getSinkPads().get(0)));
        log.warning("aRecordConvert->aRecordEnc->recordMux: "
        		+Element.linkMany(aRecordConvert, aRecordEnc, recordMux));
        log.warning("recordMux->fRecordSink: "
        		+Element.linkMany(recordMux, fRecordSink));
        
        vRecordQueue.setState(State.PAUSED);
		vRecordScale.setState(State.PAUSED);
		vRecordScaleCaps.setState(State.PAUSED);
		vRecordCsp.setState(State.PAUSED);
		vRecordEnc.setState(State.PAUSED);
		aRecordQueue.setState(State.PAUSED);
		aRecordConvert.setState(State.PAUSED);
		aRecordEnc.setState(State.PAUSED);
		recordMux.setState(State.PAUSED);
		fRecordSink.setState(State.PAUSED);
		
		//Video linkin'
		Pad vTeePad = vTee.getRequestPad("src%d");
		if (vTeePad == null) {
			throw new GstreamerException("Cannot request a pad from tee");
		}
		if (!vTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		vTeePad.link(vRecordQueue.getSinkPads().get(0));
		if (!vTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");
		}

		//Audio linkin'
		Pad aTeePad = aTee.getRequestPad("src%d");
		if (aTeePad == null) {
			throw new GstreamerException("Cannot request a pad from tee");
		}
		if (!aTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		aTeePad.link(aRecordQueue.getSinkPads().get(0));
		if (!aTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");
		}

	}
	
	public void stopRecording() throws GstreamerException {
		vRecordQueue.sendEvent(new EOSEvent());
		aRecordQueue.sendEvent(new EOSEvent());

		Pad aTeePad = aRecordQueue.getSinkPads().get(0).getPeer();
		if (aTeePad == null) {
			throw new GstreamerException("Cannot get aTeepad");			
		}
		if (!aTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block aTee pad");			
		}
		if (!aTeePad.unlink(aRecordQueue.getSinkPads().get(0))) {
			throw new GstreamerException("aSinkQueue cannot be unlinked from aTee");
		}
		if (!aTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock aTee pad");	
		}
		aTee.releaseRequestPad(aTeePad);
		
		Pad vTeePad = vRecordQueue.getSinkPads().get(0).getPeer();
		if (vTeePad == null) {
			throw new GstreamerException("Cannot get vTeepad");			
		}
		if (!vTeePad.setBlocked(true)) {
			throw new GstreamerException("Cannot block vTee pad");			
		}
		if (!vTeePad.unlink(vRecordQueue.getSinkPads().get(0))) {
			throw new GstreamerException("vSinkQueue cannot be unlinked from vTee");
		}
		if (!vTeePad.setBlocked(false)) {
			throw new GstreamerException("Cannot unblock vTee pad");	
		}
		vTee.releaseRequestPad(vTeePad);
				
		//TODO: Determine when EOS Event arrived at fRecordSink, and wait for it
		Element.unlinkMany(vRecordScale, vRecordScaleCaps, vRecordCsp, vRecordEnc, recordMux);
        Element.unlinkMany(aRecordConvert, aRecordEnc, recordMux);
        Element.unlinkMany(recordMux, fRecordSink);
       
		pipe.removeMany(vRecordScale, vRecordScaleCaps, vRecordEnc, aRecordConvert, aRecordEnc, recordMux, fRecordSink);
		
		vRecordQueue.setState(State.NULL);
		vRecordScale.setState(State.NULL);
		vRecordScaleCaps.setState(State.NULL);
		vRecordCsp.setState(State.NULL);
		vRecordEnc.setState(State.NULL);
		aRecordQueue.setState(State.NULL);
		aRecordConvert.setState(State.NULL);
		aRecordEnc.setState(State.NULL);
		recordMux.setState(State.NULL);
		fRecordSink.setState(State.NULL);
		
		recordingInProgress = false;
	}
	
	public boolean isRecordingInProgress() {
		return recordingInProgress;
	}

	public Format getRecordingFormat() {
		return recordingFormat;
	}

	public Resolution getRecordingResolution() {
		return recordingResolution;
	}

	public String getRecordingPath() {
		return recordingPath;
	}

	public AudioDevice getAudioOutputDevice() {
		return aOutputDevice;
	}
	
	public AudioDevice getAudioInputDevice() {
		return aInputDevice;
	}
	
	public VideoDevice getVideoInputDevice() {
		return vInputDevice;
	}
	
		
	public void constructMainPipeline(AudioDevice aInputDevice, AudioDevice aOutputDevice, VideoDevice vInputDevice) throws GstreamerException {
		this.aInputDevice = aInputDevice;
		this.aOutputDevice = aOutputDevice;
		this.vInputDevice = vInputDevice;

		pipe = new Pipeline("TV pipeline");
		pipe.set("async-handling", true);
        aBin = new Bin();
        aBin.set("async-handling", true);

        if (aInputDevice == null) {
        	aSrc = ElementFactory.make("audiotestsrc", "aSrc"+nextID());
        	aSrc.set("wave", 5); //Setting pink noise
        } else {
        	aSrc = ElementFactory.make("alsasrc", "aSrc"+nextID());
        	aSrc.set("device", aInputDevice.getDeviceIdentifier());
        }
        aCaps = ElementFactory.make("capsfilter", "aCaps"+nextID());
        aCaps.setCaps(Caps.fromString("audio/x-raw-int, rate=32000"));
        aTee = ElementFactory.make("tee", "aTee"+nextID());
        aFakeSinkQueue = ElementFactory.make("queue", "aFakesinkQueue"+nextID());
        aFakeSink = ElementFactory.make("fakesink", "aFakesink"+nextID());
        aFakeSink.set("sync", true);
		aSinkQueue = ElementFactory.make("queue", "aSinkQueue"+nextID());
		aRecordQueue = ElementFactory.make("queue", "aRecordQueue"+nextID());
		aRTPQueue = ElementFactory.make("queue", "artpQueue"+nextID());

        aBin.addMany(aSrc, aCaps, aTee, aFakeSinkQueue, aFakeSink, aSinkQueue, aRecordQueue, aRTPQueue);
		if (!Element.linkMany(aSrc, aCaps, aTee)) {
			throw new GstreamerException("Cannot link aSrc, aCaps, aTee");
		}
		aTee.getRequestPad("src%d").link(aFakeSinkQueue.getSinkPads().get(0));
		if (!Element.linkMany(aFakeSinkQueue, aFakeSink)) {
			throw new GstreamerException("Cannot link aFakeSinkQueue to aFakesink");
		}
        if (aOutputDevice != null) {
        	aSink = ElementFactory.make("alsasink", "aSrc"+nextID());
        	aSink.set("device", aOutputDevice.getDeviceIdentifier());
        	aBin.add(aSink);
    		aTee.getRequestPad("src%d").link(aSinkQueue.getSinkPads().get(0));
    		if (!Element.linkMany(aSinkQueue, aSink)) {
    			throw new GstreamerException("Cannot link aFakeSinkQueue to aFakesink");
    		}	
        }
        //aTee.getRequestPad("src%d").link(aGPQueue.getSinkPads().get(0));
        aBin.addPad(new GhostPad("record_src", aRecordQueue.getSrcPads().get(0)));
        aBin.addPad(new GhostPad("rtp_src", aRTPQueue.getSrcPads().get(0)));

        
		//---------------------------------------------------//
		
        //Creating video bins and elements
		vBin = new Bin();
        vBin.set("async-handling", true);
        if (vInputDevice == null) {
        	vSrc = ElementFactory.make("videotestsrc", "vSrc"+nextID());
        } else {
        	vSrc = ElementFactory.make("v4l2src", "vSrc"+nextID());
        	vSrc.set("device", vInputDevice.getDeviceFileName());
        }
        Element vCaps = ElementFactory.make("capsfilter", "videocapsfilter"+nextID());
        vCaps.setCaps(new Caps("video/x-raw-yuv, width=720, height=578"));

        vTee = ElementFactory.make("tee", "vTee"+nextID());
        vFakeSinkQueue = ElementFactory.make("queue", "vFakeSinkQueue"+nextID());
        vFakeSink = ElementFactory.make("fakesink", "vFakesink"+nextID());
        vFakeSink.set("sync", true);
        vRecordQueue = ElementFactory.make("queue", "vRecordQueue"+nextID());
        vRTPQueue = ElementFactory.make("queue", "RTPQueue"+nextID());
        vBin.addMany(vSrc, vCaps, vTee, vFakeSinkQueue, vFakeSink, vRecordQueue, vRTPQueue);
        
        if (!Element.linkMany(vSrc, vCaps, vTee)) { 
        	throw new GstreamerException("Cannot link vSrc to vTee");
        }
        vTee.getRequestPad("src%d").link(vFakeSinkQueue.getSinkPads().get(0));
        if (!Element.linkMany(vFakeSinkQueue, vFakeSink)) { 
        	throw new GstreamerException("Cannot link vFakeSinkQueue to vFakeSink");
        }
        //vTee.getRequestPad("src%d").link(vGPQueue.getSinkPads().get(0));
        vBin.addPad(new GhostPad("record_src", vRecordQueue.getSrcPads().get(0)));
        vBin.addPad(new GhostPad("rtp_src", vRTPQueue.getSrcPads().get(0)));
        
        pipe.addMany(vBin, aBin);
        pipe.setState(org.gstreamer.State.PLAYING);

        //Checking tuner capability 
        tunerCapable = false;
        try {
	        Tuner tuner = Tuner.wrap(vSrc);
	        if (tuner != null) {
		        tc = tuner.getChannel();
		        if (tc != null && tc.isTuningChannel())
		        		tunerCapable = true;
	        }
        } catch (IllegalArgumentException e) {
        	tunerCapable = false;
        }
        
        log.fine("Device tuner capability: "+tunerCapable);
               
        //Gst.main();
        //pipe.setState(org.gstreamer.State.NULL);
        
	}
	
	public void destructMainPipeline() {
		if (pipe != null) {
			pipe.setState(org.gstreamer.State.NULL);
			pipe.disown();
			pipe.dispose();
			pipe = null;
		}
		tunerCapable = false;
	}
	
	public class GstreamerException extends Exception {
		private static final long serialVersionUID = 1L;
		public GstreamerException() {
			super();
		}
		public GstreamerException(String msg) {
			super(msg);
		}	
	}
	
	public class CannotChangeFrequencyException extends Exception {
		private static final long serialVersionUID = 1L;
		public CannotChangeFrequencyException() {
			super();
		}
		public CannotChangeFrequencyException(String msg) {
			super(msg);
		}			
	}
	
	public static int nextID() {
		return ++sequence;
	}
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(TVDataHandler.class.getName());

}
