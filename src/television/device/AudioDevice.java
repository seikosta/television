package television.device;

import org.gstreamer.Element;
import org.gstreamer.ElementFactory;

import television.TVDataHandler;

public class AudioDevice {
	private String deviceIdentifier;
	private String deviceName;
	private Element elem;

	public AudioDevice(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
		this.elem = null;
	}
	
	
	public Element getSourceElement() {
		if (elem != null) {
			return elem;
		}
		elem = ElementFactory.make("alsasrc", "audiosrc"+TVDataHandler.nextID());
		elem.set("device", deviceIdentifier);
		return elem;
	}
	
	public Element getSinkElement() {
		if (elem != null) {
			return elem;
		}
		elem = ElementFactory.make("alsasink", "audiosink"+TVDataHandler.nextID());
		elem.set("device", deviceIdentifier);
		return elem;
	}
	
	/**
	 * return true on success
	 */
	public boolean discoverDeviceCapabilities() {
		getSourceElement();
		if (elem == null) return false;
		deviceName = (String)elem.get("device-name");
		
		//delete the element, and free resources
		elem.disown();
		elem.dispose();
		elem = null;
		return true;
	}
	
	public String getDeviceName() { return deviceName; }
	public String getDeviceIdentifier() { return deviceIdentifier; } 
	
	public String toString() {
		return deviceIdentifier + " - " + deviceName;
	}

}
