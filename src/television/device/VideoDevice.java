package television.device;

import org.gstreamer.Element;
import org.gstreamer.ElementFactory;
import org.gstreamer.State;

import television.TVDataHandler;

public class VideoDevice {
	private String deviceFile;
	private String deviceName;
	private boolean hasCapture;
	private boolean hasTuner;
	private boolean hasAudio;
	private boolean isV4L2;
	private Element elem;

	public VideoDevice(String deviceFile) {
		this.deviceFile = deviceFile;
		this.elem = null;
	}
	
	
	public Element getSourceElement() {
		if (elem != null) {
			return elem;
		}
		if (isV4L2) {
			elem = ElementFactory.make("v4l2src", "vidoesrc-"+TVDataHandler.nextID());
		} else {
			elem = ElementFactory.make("v4l2src", "vidoesrc-"+TVDataHandler.nextID());
		}
		elem.set("device", deviceFile);
		return elem;
	}
	
	/**
	 * return true on success
	 */
	public boolean discoverDeviceCapabilities() {
		this.isV4L2 = true; //TODO: We have to determine whether device is v4l1, or v4l2 capable
		getSourceElement();
		if (elem == null) return false;
		
		elem.setState(State.PLAYING);
		elem.setState(State.NULL);
		
		deviceName = (String)elem.get("device-name");
		int flags = (Integer)elem.get("flags");
		hasCapture = ((flags & 0x00000001) != 0) ? true : false;
		hasTuner = ((flags & 0x00010000) != 0) ? true : false;
		hasAudio = ((flags & 0x00020000) != 0) ? true : false;
		
		//delete the element, and free resources

		elem.disown();
		elem.dispose();
		elem = null;
		
		
		return true;
	}
	
	public String getDeviceName() { return deviceName; }
	public String getDeviceFileName() { return deviceFile; }
	public boolean canCapture() { return hasCapture; }
	public boolean isTuner() { return hasTuner; }
	public boolean hasAudio() { return hasAudio; }
	
	public String toString() {
		return deviceFile + " - " + deviceName + "; Capture: " + hasCapture + ", Tuner: " + hasTuner + ", Audio:" + hasAudio;
	}

}
