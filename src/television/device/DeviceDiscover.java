package television.device;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class DeviceDiscover {
	private static ArrayList<VideoDevice> vlist;
	private static ArrayList<AudioDevice> alist;
	
	/**
	 * Constructs DeviceDiscover: initializing.
	 */
	static {
		vlist = new ArrayList<VideoDevice>();
		alist = new ArrayList<AudioDevice>();		
	}
	
	public static AudioDevice searchAudioDeviceForString(String str) {
		for (int i=0; i < alist.size(); ++i) {
			if (alist.get(i).toString().equals(str)) {
				return alist.get(i);
			}
		}
		return null;
	}
	
	public static VideoDevice searchVideoDeviceForString(String str) {
		for (int i=0; i < vlist.size(); ++i) {
			if (vlist.get(i).toString().equals(str)) {
				return vlist.get(i);
			}
		}
		return null;		
	}
	
	public static ArrayList<VideoDevice> getVideoDevices() {
		return vlist;
	}
	
	public static ArrayList<AudioDevice> getAudioDevices() {
		return alist;
	}
	
	public static void discoverDevices() {
		discoverAudioDevices();
		discoverVideoDevices();
	}

	/**
	 * Discovers available video capture devices, using gstreamer
	 * @return An Arraylist of video capture devices
	 */	
	private static void discoverVideoDevices() {
		VideoDevice vd;
		for (int i=0; i<255; ++i) {
			vd = new VideoDevice("/dev/video"+i);
			if (!(new File(vd.getDeviceFileName()).exists())) {
				break;
			}
			if (!vd.discoverDeviceCapabilities()) {
				continue;
			}
			vlist.add(vd);
		}
	}
	
	/**
	 * Discover available audio devices on the system
	 * TODO: Only alsa devices will be discovered yet.
	 * @return An ArrayList of audio capture, and playback devices
	 */	
	private static void discoverAudioDevices() {
		AudioDevice ad;
		//Adding default alsa device
		ad = new AudioDevice("default");
		ad.discoverDeviceCapabilities();
		alist.add(ad);
		try {
			BufferedReader br = new BufferedReader(new FileReader("/proc/asound/cards"));
			//parsing /proc/asound/cards
			String line;
			String[] strArr;
			int deviceNumber;
			while ((line = br.readLine()) != null) {
				line = line.substring(1); //chopping the leading space
				strArr = line.split(" ");
				if (strArr.length > 0) {
					try {
						deviceNumber = Integer.parseInt(strArr[0]);
					} catch (NumberFormatException e) {
						continue;
					}
					ad = new AudioDevice("hw:"+deviceNumber+",0");
					if (!ad.discoverDeviceCapabilities()) {
						continue;
					}
					alist.add(ad);
				}
			}
		} catch (IOException e) {
			log.warning("Can't get alsa card list");
			return;
		}
	}
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(DeviceDiscover.class.getName());

}
