package television.record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Format {
	private Container container;
	private AudioEncoder audio;
	private VideoEncoder video;
	
	private final static Map<Container, Set<Object>> formatPairs = new HashMap<Container, Set<Object>>();
	
	static {
		HashSet<Object> formats; 
		formats =  new HashSet<Object>();
		formats.add(AudioEncoder.MP3);
		formats.add(VideoEncoder.RAW);
		formats.add(VideoEncoder.XVID);
		formats.add(VideoEncoder.H264);
		formatPairs.put(Container.AVI, formats);
		
		formats =  new HashSet<Object>();
		formats.add(AudioEncoder.MP3);
		formats.add(AudioEncoder.VORBIS);
		formats.add(VideoEncoder.THEORA);
		formats.add(VideoEncoder.XVID);
		formats.add(VideoEncoder.H264);
		formats.add(VideoEncoder.RAW);
		formatPairs.put(Container.MKV, formats);
		
		formats =  new HashSet<Object>();
		formats.add(AudioEncoder.VORBIS);
		formats.add(VideoEncoder.THEORA);
		formatPairs.put(Container.OGG, formats);		
	}
	
	public static ArrayList<Format> getValidFormats() {
		ArrayList<Format> ret = new ArrayList<Format>();
		for (Container c: Container.values()) {
			for (AudioEncoder a: AudioEncoder.values()) {
				for (VideoEncoder v: VideoEncoder.values()) {
					if (formatPairs.get(c).contains(a) 
							&& formatPairs.get(c).contains(v)) {
						ret.add(new Format(c, a, v));
					}
				}
			}
		}
		return ret;
	}
	
	private Format(Container container, AudioEncoder audio, VideoEncoder video) {
		this.container = container;
		this.audio = audio;
		this.video = video;
	}	
	
	public Container getContainer() {
		return container;
	}

	public AudioEncoder getAudio() {
		return audio;
	}

	public VideoEncoder getVideo() {
		return video;
	}
	
	public String toString() {
		return container + " [" + audio + ", " + video + "]";
	}
	
	@Override
	public final boolean equals(Object other) {
		if (!(other instanceof Format))
			return false;
		Format oFormat = (Format)other;
		return (container == oFormat.getContainer() 
				&& audio == oFormat.getAudio() 
				&& video == oFormat.getVideo()); 
	}

	public enum Container {
		OGG ("Ogg Container (.ogg)", "oggmux", "ogg"), 
		MKV ("Matroska Container (.mkv)", "matroskamux", "mkv"),
		AVI ("Avi Container (.avi)", "avimux", "avi");
		
		
		String name, muxer, extension;
		private Container(String name, String muxer, String extension) {
			this.name = name;
			this.muxer = muxer;
			this.extension = extension;
		}
		
		public String getName() {
			return name;
		}
		
		public String getMuxer() {
			return muxer; 
		}
		
		public String getExtension() {
			return extension;
		}
		
		public String toString() {
			return name;
		}
	};
	
	public enum AudioEncoder {
		VORBIS ("Vorbis audio encoder", "vorbisenc"), 
		MP3 ("Mpeg1 layer3 audio encoder (mp3)", "lame"),
		AAC ("Advanced Audio Encoding (aac)", "faac");
		
		
		String name, encoder;
		private AudioEncoder(String name, String encoder) {
			this.name = name;
			this.encoder = encoder;
		}
		
		public String getName() {
			return name;
		}
		
		public String getEncoder() {
			return encoder; 
		}
		
		public String toString() {
			return name;
		}
	};
	
	public enum VideoEncoder {
		THEORA ("Theora video encoder", "theoraenc"), 
		H264 ("H264 video encoder", "x264enc"),
		XVID ("Xvid video encoder", "xvidenc"),
		RAW ("RAW video", "identity");
		
		
		String name, encoder;
		private VideoEncoder(String name, String encoder) {
			this.name = name;
			this.encoder = encoder;
		}
		
		public String getName() {
			return name;
		}
		
		public String getEncoder() {
			return encoder; 
		}
		
		public String toString() {
			return name;
		}
	};

}
