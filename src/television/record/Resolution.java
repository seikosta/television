package television.record;

public class Resolution {
	private int width;
	private int height;
	
	public Resolution(int w, int h) {
		this.width = w;
		this.height = h;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public final boolean equals(Object other) {
		if (!(other instanceof Resolution))
			return false;
		Resolution oResolution = (Resolution)other;
		return (width == oResolution.getWidth()
				&& height == oResolution.getHeight());
	};
	
	@Override
	public String toString() {
		return width+"x"+height;
	}
}
