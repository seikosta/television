package television;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;


public class Configuration extends Properties {
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;
	
	private static Configuration configuration;
	private static File confFile;
	static {
		configuration = new Configuration();
	}
	
	public static Configuration getInstance() {
		return configuration;
	}
	
	public File getConfigFile() {
		return confFile;
	}
	public void read() throws IOException {
		String userDir = System.getProperty("user.home");
		String confFileName = userDir+"/.television/television.conf";
		confFile = new File(confFileName);
		if (!confFile.isFile()) { //we need to create our config file.
			new File(userDir+"/.television").mkdir();
			confFile.createNewFile();
			log.info("New config file created under: "+confFile.getPath());
		}
		this.load(new FileReader(confFile));
	}
	
	public void store() throws IOException {
		FileWriter fw = new FileWriter(confFile);
		this.store(fw, "television configuration file");
		fw.flush();
		fw.close();
	}
	

	public boolean hasChannelWithName(String name) {
		for (Object key: keySet()) {
			String k = (String)key;
			if (k.startsWith("Channel_") 
					&& get(k).equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(Configuration.class.getName());

}
