package television.rtsp;

import java.util.Date;
import java.util.UUID;

public class RTPSessionData {
	private static final long serialVersionUID = 50000L;
	
	private String sourceId = null;
	private int clockRate = 90000;
	private String clientAddress = null;
	private String serverAddress = null;
	private int rtpClientPort = -1;
	private int rtcpClientPort = -1;
	private int rtcpServerPort = -1;
	
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public int getClockRate() {
		return clockRate;
	}
	public String getPeerAddress() {
		return clientAddress;
	}
	public void setPeerAddress(String peerAddress) {
		this.clientAddress = peerAddress;
	}
	public int getRtpClientPort() {
		return rtpClientPort;
	}
	public void setRtpClientPort(int rtpPort) {
		this.rtpClientPort = rtpPort;
	}
	public int getRtcpServerPort() {
		return rtcpServerPort;
	}
	public void setRtcpServerPort(int rtcpServerPort) {
		this.rtcpServerPort = rtcpServerPort;
	}
	public int getRtcpClientPort() {
		return rtcpClientPort;
	}
	public void setRtcpClientPort(int rtcpClientPort) {
		this.rtcpClientPort = rtcpClientPort;
	}
	public String getServerAddress() {
		return serverAddress;
	}
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	
	public String createSDP(){
	
		UUID sessionID = UUID.randomUUID();
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("v=0\n"); //FIX SDP version
		builder.append("o=- "+sessionID+" 1 IN IP4 " + serverAddress +"\n"); //GENERATED o=<username> <session id> <version> <network type> <address type> <address>

		builder.append("s=Session streamed with gstreamer\n"); //GENERATED SDP session name *1
		builder.append("i=rtsp-server\n"); //FIX Session description
		//builder.append("e=NONE\n"); //FIX Email address
		//builder.append("p=00 5555 555\n"); //FIX Phone Number
		
		long ntpStart = 2208988800L;
		long ntpNow = new Date().getTime() + 2208988800L;
		
		builder.append("t="+ntpStart+" "+ntpNow+"\n"); //FIX <starttime> <stoptime>
		builder.append("a=tool:GStreamer\n"); //FIX SessionAttribute 
		builder.append("a=type:broadcast\n"); //FIX SessionAttribute | broadcast is the default type by RFC
		
		//MediaDesriptions
		//TODO: Ha lesz tobb encodolasu stream akkor itt javitani kell
		builder.append("m=video 0 RTP/AVP 96\n"); //GENERATED (audio|video) (port) (protocol) (payload szama, tablazatbol)
		builder.append("c=IN IP4 "+ serverAddress+"\n"); //GENERATED server address
		builder.append("a=rtpmap:96 H264/90000\n");
		//builder.append("a=rtpmap:96 H264/90000\n"); //GENERATED MediaAttributes
		builder.append("a=control:stream=0\n"); //GENERATED MediaAttributes
		//GENERATED MediaAttributes
		//builder.append("a=fmtp:96 profile-level-id=4d4033;sprop-parameter-sets=Z01AM5JUAoAi2AiAAAAyAAALtUeMGVA=,a048gA==\n");
	
		return builder.toString();
	}
	
	/**
	 * Generates Gstreamer capabilities string for setting the caps of an UDP source element
	 * looks like: "application/x-rtp,media=(string)video,clock-rate=(int)90000,encoding-name=(string)H264"
	 * @return The GStreamer caps string.
	 */
	public String createUdpSrcCapsData() {
		return "application/x-rtp,media=(string)video,clock-rate=(int)"+getClockRate()+",encoding-name=(string)H264";
	}
		
	@Override
	public Object clone() throws CloneNotSupportedException {	//This has to be here, see Cloneable documentation 
		return super.clone();
	};
	
	@Override
	public String toString() {		//Not string builder because this method is only used by testing
		return "Source URI: " + sourceId + "\n" +
			"clockRate: " + clockRate + "\n" +
			"peerAddress: " + clientAddress + "\n" +
			"serverAddress: " + serverAddress + "\n" +
			"rtpReceiverPort: " + rtpClientPort + "\n" +
			"rtcpSenderPort: " + rtcpServerPort + "\n" +
			"rtcpReceiverPort: " + rtcpClientPort;
	}

}
