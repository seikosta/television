package television.rtsp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.naming.ConfigurationException;

import television.Configuration;
import television.TVDataHandler;
import television.TVDataHandler.CannotChangeFrequencyException;
import television.TVDataHandler.GstreamerException;
import television.channel.Channel;

public class RTSPSession extends Thread {
	private static final String RTSPVERSION = "RTSP/1.0"; //Supported Version 
	private static final String ACCEPTHEADER = "Accept";
	private static final String TRANSPORTHEADER = "Transport";
	
	private String channelID = null;
	private BufferedReader in;
	private BufferedWriter out;
	private Socket socket;
	private String sessionID;
	private final RTPSessionData rtp;
	private TVDataHandler tdh;

		
	/**
	 * Constructs the RTPSession class, initializes input/output channels
	 * @param sock Socket, through we communicate with the connected client
	 * @throws IOException
	 */
	public RTSPSession(Socket sock, TVDataHandler tdh) throws IOException {
		this.tdh = tdh;
		socket = sock;
		sessionID = UUID.randomUUID().toString();
		rtp =  RTPResource.getInstance().createServerSideData();
		
		in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
	}

	/**
	 * Sends a generic response, with the given state, without any further header and body parts
	 * @param state A Status code which the response will be sent
	 * @throws IOException
	 */
	private void handleGenericResponse(Status state) throws IOException {
		new RTSPResponse(RTSPVERSION, state).send(out);
	}
	
	/**
	 * Sends the supported options to the RTSP client. 
	 * @param request RTSPRequest, which calls this function
	 * @throws IOException
	 */
	private void handleOptionsResponse(RTSPRequest request) throws IOException {		
		RTSPResponse response = new RTSPResponse(request.getVersion(), Status.OK, request.getHeaders());
		response.getHeaders().put("Public", "OPTIONS, DESCRIBE, PLAY, SETUP, TEARDOWN");
		response.send(out);
	}
	
	/**
	 * Sends an SDP to client, which describe the requested stream
	 * @param request RTSPRequest, which calls this function
	 * @throws IOException
	 * @throws RTSPProtocolFailureException
	 * @throws URISyntaxException 
	 */
	private void handleDescribeResponse(RTSPRequest request) throws IOException, RTSPProtocolFailureException, URISyntaxException {
		if (request.getHeaders() == null || request.getHeaders().get(ACCEPTHEADER) == null){
			handleGenericResponse(Status.BAD_REQUEST);
			throw new RTSPProtocolFailureException("Bad Request");
		}
		if (!request.getHeaders().get("Accept").equals("application/sdp")) {
			log.warning("Client doesn't support sdp");
			handleGenericResponse(Status.OPTION_NOT_SUPPORTED);
			throw new RTSPProtocolFailureException("Option not Supported");
		}
		
		String[] pathParts = request.getURI().getPath().substring(1).split("/");
		
		/*
		HashMap<String, String> uriConfig = new HashMap<String, String>();
		String[] confParts;
		for (int i=1; i<pathParts.length; ++i) { //First element is left out, cause that is channel id
			confParts = pathParts[i].split("=");
			uriConfig.put(confParts[0], confParts[1]);
		}
		*/
		channelID = pathParts[0];
		
		/*
		if (pathParts[1].startsWith(StreamCapsConf.SETTING_PREFIX)) { // live view
			rtp.setCaps(ConfigurationManager.getGlobal().getStreamCapsById(pathParts[1]));
			startTimeStamp = 0; //means: live view
		} else { // archive view
			rtp.setCaps(ConfigurationManager.getGlobal().getStreamCapsById("configuration.test.rtp"));
			//TODO: Set appropriate encoding!!! 
			startTimeStamp = Integer.parseInt(pathParts[1]); // Unix Timestamp (UTC)
			if (pathParts.length < 3) {
				throw new URISyntaxException("Missing Record File Manager Session ID.", channelID, startTimeStamp);
			}
			rfmSession = pathParts[2];
		}
		*/
		
		
		RTSPResponse response = new RTSPResponse(request.getVersion(), Status.OK, request.getHeaders());
		response.setBody(rtp.createSDP());
		response.send(out);
	}
	
	/**
	 * Negotiate server and client ports for RTP
	 * @param request RTSPRequest, which calls this function
	 * @throws IOException
	 * @throws RTSPProtocolFailureException
	 */
	private void handleSetupResponse(RTSPRequest request) throws IOException, RTSPProtocolFailureException {
		if (!request.getHeaders().containsKey(TRANSPORTHEADER)) {
			handleGenericResponse(Status.BAD_REQUEST);
			throw new RTSPProtocolFailureException("Bad Request");
		}
		String transportHeaderIn = request.getHeaders().get(TRANSPORTHEADER);
		String transportHeaderOut = "";
		String[] trans_header = transportHeaderIn.split(";");
		int transHeadFlags = 0;
		for (int i=0; i<trans_header.length; ++i) {
			if ((trans_header[i].startsWith("RTP/AVP")) && ((transHeadFlags & 0x01)==0)) {
				transportHeaderOut += trans_header[i] + ";";
				transHeadFlags |= 0x01;
			} else if ((trans_header[i].startsWith("unicast")) && ((transHeadFlags & 0x02)==0)){
				transportHeaderOut += trans_header[i] + ";";
				transHeadFlags |= 0x02;
			} else if ((trans_header[i].startsWith("client_port")) && ((transHeadFlags & 0x04)==0)) {
				String[] portsMessage = trans_header[i].split(",");
				transportHeaderOut += portsMessage[0] + ";";
				String[] ports = (portsMessage[0].split("="))[1].split("-");
				rtp.setRtpClientPort(Integer.parseInt(ports[0]));
				rtp.setRtcpClientPort(Integer.parseInt(ports[1]));
				transHeadFlags |= 0x04;
			}
		}
		//We don't have all the required header instances
		if (transHeadFlags != 7) {
			handleGenericResponse(Status.BAD_REQUEST);
			throw new RTSPProtocolFailureException("Bad Request\n ");
		}
		//generating response, and configuring headers
		RTSPResponse response = new RTSPResponse(request.getVersion(), Status.OK, request.getHeaders());
		//response.getHeaders().put("Session", "uniquesessionname");
		response.getHeaders().put("Session", sessionID);
		//transportHeaderOut += "server_port=" + (rtp.getRtcpServerPort()) + "-" + rtp.getRtcpServerPort() + ",RTP/AVP/UDP;mode=\"PLAY\""; 
		transportHeaderOut += "server_port=" + (rtp.getRtcpServerPort()) + "-" + rtp.getRtcpServerPort() + ";mode=\"PLAY\""; 
		response.getHeaders().put("Transport", transportHeaderOut); 
		
		response.send(out);
	}
	
	/**
	 * Starts the selected RTP stream
	 * @param request RTSPRequest, which calls this function
	 * @throws IOException
	 * @throws RTSPProtocolFailureException
	 * @throws CannotChangeFrequencyException 
	 * @throws GstreamerException 
	 * @throws ConfigurationException
	 * @throws StreamException 
	 */
	private void handlePlayResponse(RTSPRequest request) throws IOException, RTSPProtocolFailureException, CannotChangeFrequencyException, GstreamerException {
		if (!channelID.equals("") 
				&& !Configuration.getInstance().hasChannelWithName(channelID)) {
			handleGenericResponse(Status.NOT_FOUND);
			throw new RTSPProtocolFailureException("URI not found");
		}
		rtp.setSourceId(channelID);
		rtp.setPeerAddress(socket.getInetAddress().getHostAddress().toString());
		
		if (!channelID.equals("")) {
			tdh.setFreqency(Channel.valueOf(channelID).getFreqHz());
		}
		
		tdh.startRTP(rtp.getPeerAddress(), rtp.getRtpClientPort(), rtp.getRtcpServerPort(), rtp.getRtcpClientPort());
		
		RTSPResponse response = new RTSPResponse(request.getVersion(), Status.OK, request.getHeaders());
		//response.getHeaders().put("RTP-Info", "url=rtsp://localhost:1554/0/stream=0;seq=0;rtptime=0");
		response.send(out);	
	}
	
	/**
	 * Stops the RTP stream
	 * @param request RTSPRequest, which calls this function
	 * @throws IOException
	 * @throws GstreamerException 
	 * @throws ConfigurationException
	 */
	private void handleTeardownResponse(RTSPRequest request) throws IOException, GstreamerException {
		//TODO: Stops RTP stream at TVDataHandler
		handleGenericResponse(Status.OK);
		tdh.stopRTP();
	}
	
	/**
	 * The RTSPSessions main loop function. Manages communications with the client.
	 */
	public void run() {
		try {
			String inputStr;
			while (socket.isConnected() && ((inputStr = in.readLine()) != null) ) {
				RTSPRequest request = new RTSPRequest();
				request.parseRequest(inputStr);
				//We accept only RTSP version 1.0
				if (!request.version.equals(RTSPVERSION)) {
					log.warning("RTSP version Not supported: "+request.getVersion());
					new RTSPResponse("RTSP/1.0", Status.RTSP_VERSION_NOT_SUPPORTED).send(out);
				}
				
				try {
					if (request.method.equals("OPTIONS")) {
						handleOptionsResponse(request); 
					} else if (request.method.equals("DESCRIBE")) {
						handleDescribeResponse(request);
					} else if (request.method.equals("SETUP")) {
						handleSetupResponse(request);
					} else if (request.method.equals("PLAY")) {
						handlePlayResponse(request);
					} else if (request.method.equals("TEARDOWN")) {
						handleTeardownResponse(request);
						break;
					} else {
						handleGenericResponse(Status.BAD_REQUEST);
					}
				} catch (RTSPProtocolFailureException e) {
					//We don't have to close the connection in that case of error
					//just logging, that we can't handle some message
					log.warning("Protocol Failure: "+e);
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			log.warning("IO error: "+e);
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			log.warning("Bad format of URI: "+e);
		} catch (CannotChangeFrequencyException e) {
			log.warning("Cannot chaneg channel");
		}catch (GstreamerException e) {
			log.warning("Gstreamer error: "+e);
		} finally {
			try {
				//TODO: stop playback at TV Data Handler
				socket.close();			
			} catch (Exception e) {
				log.warning("We can't close RTSP Session socket" + e);
			} 
		}
	}

	/**
	 * Holding RTSP Header info as key-value pairs.
	 * @author kotyo
	 *
	 */
	private class RTSPHeaders extends HashMap<String, String> {
		private static final long serialVersionUID = 4559689095261719745L;

		public String toString() {
			StringBuilder str = new StringBuilder();
		    Iterator<Entry<String, String>> it = entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
		        str.append(pairs.getKey());
		        str.append(": ");
		        str.append(pairs.getValue());
		        str.append("\n");
		    }
		    return str.toString();
		}
	}
	
	/**
	 * Reads and holds an RTSP request.
	 * @author kotyo
	 *
	 */
	private class RTSPRequest {
		private String method = null;
		private URI uri = null;
		private String version = null;
		private RTSPHeaders headers = null;
		private String body = null;
		
		public RTSPRequest() { 
			uri = null;
			headers = new RTSPHeaders();
		}
		
		public void parseRequest(String buff) throws IOException, URISyntaxException {
			String[] parts = buff.split(" ");

			log.finer("<m< "+buff);
			method = parts[0];
			uri = new URI(parts[1]);
			version = parts[2];
			
			//Parsing headers
			while (!(buff = in.readLine()).isEmpty()) {
				parts = buff.split(": ");
				headers.put(parts[0], parts[1]);
			}
			log.finer("Headers IN: \n"+headers);
			
			//Reading body
			if (headers.containsKey("Content-Length")) {
				in.read(body.toCharArray(), 0, Integer.parseInt(headers.get("Content-Length")));
			}
		}
		
		public String getMethod() { return method; }
		public URI getURI() { return uri; }
		public String getVersion() { return version; }
		public RTSPHeaders getHeaders() { return headers; }
		public String getBody() { return body; }
		
		public String toString() {
			StringBuffer buff = new StringBuffer();
			buff.append("[");
			buff.append(method);
			buff.append("]");			
			buff.append(uri);
			buff.append("\n");
			buff.append(headers);
			buff.append(body);
			
			return buff.toString();
		}
	}
	
	/**
	 * Holds and sends an RTSP response
	 * @author kotyo
	 *
	 */
	private class RTSPResponse {
		private Status status;
		private RTSPHeaders headers;
		private String version;
		private String body;
		
		public RTSPResponse(String version, Status state) {
			this.status = state;
			this.version = version;
			body = "";
			headers = new RTSPHeaders();
		}
		
		public RTSPResponse(String version, Status state, RTSPHeaders headers) {
			this.status = state;
			this.version = version;
			this.headers = (RTSPHeaders)headers.clone();
			body = "";
		}
		
		public void send(BufferedWriter out) throws IOException {
			//handling Content-Length header
			StringBuilder str = new StringBuilder(); 
			if (body.length() > 0) {
				headers.put("Content-Length", ""+body.length());
			}
			log.finer(">m> "+version+" "+status);
			log.finer("Headers OUT: \n" + headers);
			str.append(version);
			str.append(" ");
			str.append(status);
			str.append("\n");
			str.append(headers);
			str.append("\n");
			str.append(body);
			out.write(str.toString());
			out.flush();
		}
		
		public RTSPHeaders getHeaders() { return headers; }
		public void setBody(String str) { body = str; }
		public String getBody() { return body; }
		public Status getStatus() { return status; }
		
	}

	/**
	 * RTSP Status codes and messages
	 * @author kotyo
	 *
	 */
	public enum Status {
		OK (200, "OK"),
		BAD_REQUEST (400, "Bad Request"),
		NOT_FOUND (404, "Not Found"),
		RTSP_VERSION_NOT_SUPPORTED (505, "RTSP Version not supported"),
		OPTION_NOT_SUPPORTED (555, "Option Not Supported");
		
		private final int code;
		private final String message;
		
		Status (int code, String message) {
			this.code = code;
			this.message = message;
		}
		public int getCode() { return code; }
		public String getMessage() { return message; }
		public String toString() { return code + " " + message; }
	}
	
	/**
	 * This exception generated, when RTSP server got an unexpected message from the client 
	 * @author kotyo
	 *
	 */
	public class RTSPProtocolFailureException extends Exception {
		private static final long serialVersionUID = 5033708538940120201L;
		public RTSPProtocolFailureException() { }
		public RTSPProtocolFailureException(String msg) {
			super(msg);
		}
	}
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(RTSPSession.class.getName());

}
