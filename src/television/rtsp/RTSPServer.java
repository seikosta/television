package television.rtsp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import javax.net.ServerSocketFactory;

import television.TVDataHandler;
import television.rtsp.RTSPSession;
public class RTSPServer extends Thread{
	public static final int DEFAULT_PORT = 1554;
	private final TVDataHandler tdh;
	private int port = DEFAULT_PORT;
	
	public RTSPServer(int port, final TVDataHandler tdh) {
		this.tdh = tdh;
		if (port > 0)
			this.port = port;
	}
	
	public int getPort() {
		return port;
	}
	
	public void run() {
		Socket clientSocket = null;
		ServerSocketFactory factory;
		try {
			factory = ServerSocketFactory.getDefault();
			//ServerSocketFactory factory = SSLServerSocketFactory.getDefault();
			ServerSocket ss = factory.createServerSocket(port);
			while (true) {
				try {
					clientSocket = ss.accept();
					new RTSPSession(clientSocket, tdh).start();	//Creates a new session object for all new connections
				} catch (IOException e) {
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (clientSocket != null && !clientSocket.isClosed())
					clientSocket.close();
			} catch (IOException e) {
				log.warning("Can't close RTSP client socket");						
			}
		}
	}

	private static Logger log = Logger.getLogger(RTSPServer.class.getName());

}
