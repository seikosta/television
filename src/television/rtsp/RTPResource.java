package television.rtsp;

import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class RTPResource {
	
	/**
	 * From where the port numbers will be utilized
	 */
	private static final int CLIENT_PORT_BASE = 5000;
	//private static final int MAX_CLIENT_PORT =  5999;
	private static final int SERVER_PORT_BASE = 6000;
	//private static final int MAX_SERVER_PORT =  6999;

	private static final RTPResource instance = new RTPResource();

	private int nextClientPort = CLIENT_PORT_BASE;
	private int nextServerPort = SERVER_PORT_BASE;
	
	/**
	 * Private constructor
	 */
	private RTPResource(){}
	
	public static RTPResource getInstance(){
		return instance;
	}
		
	/**
	 * Registers the new session. This is the server side. 
	 * Fills the SessionData with:<br><ul>
	 * <li>The sender rtcp port.</li>
	 * <li>The server address.</li> 
	 * </ul>
	 * @throws UnknownHostException 
	 */
	public RTPSessionData createServerSideData(){
		RTPSessionData result = new RTPSessionData();
		//Find a free port
		result.setRtcpServerPort(findFreeUdpPort(true)+1);
		//Sender address
		try {
			result.setServerAddress( Inet4Address.getLocalHost().getHostAddress() );
		} catch (UnknownHostException e) {
			//This should never happen, because localhost is always known host.
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * Fills an RtpSession data object with the basic enrties:
	 * Client side call
	 * rtp, rtcp ports, capabilities, uri
	 */
	public RTPSessionData createInStreamData(String uri){
		RTPSessionData result = new RTPSessionData();
		result.setSourceId(uri);
		try {
			result.setPeerAddress(Inet4Address.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		result.setRtcpClientPort(findFreeUdpPort(false));
		result.setRtpClientPort(findFreeUdpPort(false));
		return result;
	}
	
	//Returns the server address of the local machine
	public String getServerAddress() {
		try {
			return InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			//We are asking for localhost. It will be always known. But if not...
			log.warning("Couldn't get own address");
			e.printStackTrace();
			return "127.0.0.1";
		} 
	}
	
	/**
	 * Searches for a free UDP port
	 * @return Returns the port number
	 */
	public int findFreeUdpPort(boolean isServer){	//TODO: ha túlfut, ha nem talált, ...
		DatagramSocket sock = null;
		boolean okay = false;
		int port = -1;
		while (!okay){
			try{
				int tempPort = isServer ? nextServerPort : nextClientPort; 
				sock = new DatagramSocket(tempPort);
				sock.close();
				okay = true;
				port = tempPort;
				if (isServer)
					nextServerPort+=2;
				else 
					nextClientPort+=2;
			} catch (SocketException e) {
				okay = false;
				nextClientPort+=2;
			}
		}
		return port;
	}
	private static Logger log = Logger.getLogger(RTPResource.class.getName()); 
}
