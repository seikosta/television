package television.channel;

import java.util.Iterator;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ProgressBar;

import television.Configuration;
import television.TVDataHandler;
import television.TVDataHandler.CannotChangeFrequencyException;


public class ChannelDetect {
	private int chFound = 0;
	private TVDataHandler tdh;
	private static final int STATE_STOPPED = 0;
	private static final int STATE_RUNNING = 1;
	private static final int STATE_STOPPING = 2;
	private static final String START_SETECTION_TEXT = "Start Channel Detection";
	private static final String STOP_SETECTION_TEXT = "Stop Channel Detection";

	private Integer state = STATE_STOPPED;
	private Button button = null;
	private ProgressBar progress = null;
	private Detection detection;
	private Display display;
	
	public ProgressBar getProgress() {
		return progress;
	}

	public void setProgress(ProgressBar progress) {
		this.progress = progress;
	}

	public Button getButton() {
		return button;
	}

	public void setButton(final Button button) {
		this.button = button;
		button.setText(START_SETECTION_TEXT);
		
		Listener autodetectButtonListener = new Listener() {
			public void handleEvent(Event event) {
				if (event.widget == button) {
					synchronized(state) {
						if (state == STATE_STOPPED) {
							startDetect();
						} else if (state == STATE_RUNNING) {
							stopDetect();						
						}
					}
				}
			}
		};
		button.addListener(SWT.Selection, autodetectButtonListener);

	}

	public ChannelDetect(TVDataHandler tdh, Display disp) {
		this.tdh = tdh;
		display = disp;
	}
	
	public void startDetect() {
		synchronized (state) {
			if (state == STATE_STOPPED) {
				state = STATE_RUNNING;
				button.setText(STOP_SETECTION_TEXT);
				button.getParent().layout(true);
				detection = new Detection();
				detection.start();
			}
		}
	}
	
	public void stopDetect() {
		synchronized (state) {
			if (state == STATE_RUNNING) {
				state = STATE_STOPPING;
				button.setEnabled(false);
			} else {
				return;
			}
		}
		try {
			detection.join();
		} catch (InterruptedException ex){
			//TODO handle it somehow
		} finally {
			detection = null;
			button.setEnabled(true);
			button.setText(START_SETECTION_TEXT);
			button.getParent().layout(true);
			progress.setSelection(0);
		}
	}
	
	private class Detection extends Thread {
		private Configuration conf = Configuration.getInstance();
		
		private void storeChannel(Channel c) {
			conf.setProperty("Channel_"+ chFound++, c.name());
		}

		public void run() {
			Channel c;
			
			//Doing some cleanup in the config
			for (Iterator<Object> itr=conf.keySet().iterator(); itr.hasNext();) {
				if (((String)itr.next()).startsWith("Channel_"))
					itr.remove();
			}
			
			display.asyncExec(new Runnable() {
				@Override
				public void run() {
					if (progress != null) {
						progress.setMinimum(0);
						progress.setMaximum(Channel.values().length);
					}					
				}
			});
			
				
			try {
				for (int i=0; i<Channel.values().length; ++i) {
					final int ii = i;
					display.asyncExec(new Runnable(){
						public void run() {
							if (progress != null) {
								progress.setSelection(ii);
								progress.redraw();
							}							
						}
					});
					
					synchronized(state) {
						if (state != STATE_RUNNING) {
							state = STATE_STOPPED;
							return;
						}
					}
					c = Channel.values()[i];
					tdh.setFreqency(c.getFreqHz());
					try {
						Thread.sleep(1000);
					} catch (Exception e) {}
					log.info("Trying: "+ c.name() + "@ " + c.getFreqMHz() + "MHz, signal strength: " + tdh.getSignalStrength());
					if (tdh.getSignalStrength() > 0) {
						storeChannel(c);
					}
				}
			} catch (CannotChangeFrequencyException ex) {
				//TODO handle it somehow
			} finally {
				state = STATE_STOPPED;
				display.asyncExec(new Runnable(){
					@Override
					public void run() {
						button.setEnabled(true);
						button.setText(START_SETECTION_TEXT);
						progress.setSelection(0);
					}
				});
			}
		}
	};
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(ChannelDetect.class.getName());
}
