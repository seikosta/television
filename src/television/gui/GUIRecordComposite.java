package television.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.gstreamer.Element;
import org.gstreamer.swt.overlay.VideoComponent;

import television.TVDataHandler;
import television.TVDataHandler.GstreamerException;
import television.record.Format;
import television.record.Resolution;

public class GUIRecordComposite extends Composite {
	private TVDataHandler tdh;
	private final Combo resCombo;
	private final Combo formatCombo;
	private final Text fileText;
	private Element vcEl = null;
	private VideoComponent vc = null;
	private final ArrayList<Resolution> resolutions = new ArrayList<Resolution>();
	private final ArrayList<Format> formats = Format.getValidFormats();
	private File recordFile = new File("/tmp/television.ogg");
	
	public GUIRecordComposite(Composite parent, int style, final TVDataHandler tdh, final Shell shell) {
		super(parent, style);
		
		//Filling resolution options
		resolutions.add(new Resolution(720, 578));
		resolutions.add(new Resolution(640, 480));
		resolutions.add(new Resolution(320, 240));
		resolutions.add(new Resolution(160, 120));
		
		this.tdh = tdh;
		
		this.setLayout(new GridLayout(2, false));
		this.layout(true);
		GridData gdThis = new GridData(GridData.FILL_BOTH);
		gdThis.widthHint = 640;
		gdThis.heightHint = 480;
		gdThis.grabExcessHorizontalSpace = true;
		gdThis.grabExcessVerticalSpace = true;
		this.setLayoutData(gdThis);

		if (tdh.getRecordingPath() != null && !tdh.getRecordingPath().isEmpty()) {
			recordFile = new File(tdh.getRecordingPath());
		}
		
		new Label(this, SWT.RIGHT_TO_LEFT).setText("Place record file to ");
		
		fileText = new Text(this, SWT.BORDER);
		fileText.setText(recordFile.getAbsolutePath());
		
		fileText.addListener(SWT.MouseDown, new Listener(){
			@Override
			public void handleEvent(Event arg0) {
			    FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			    dialog.setFilterNames(new String[] { "All Files (*.*)" });
			    dialog.setFilterExtensions(new String[] { "*.*" });
			    
			    dialog.setFileName(recordFile.getName());
			    dialog.setFilterPath(recordFile.getParent());
			    String filePath = dialog.open();
			    if (filePath == null)
			    	return;
			    recordFile = new File(filePath);
			    correctRecordFileExtension();
			}
		});
		
		new Label(this, SWT.RIGHT_TO_LEFT).setText("Record resolution ");
	    resCombo = new Combo(this, SWT.DROP_DOWN);
	    resCombo.setItems(collectionToArray(resolutions.toArray()));
	    resCombo.select(0);
	    int i = 0;
	    for (Resolution r: resolutions) {
	    	if (r.equals(tdh.getRecordingResolution())) {
	    		resCombo.select(i);
	    		break;
	    	}
	    	i++;
	    }

		new Label(this, SWT.RIGHT_TO_LEFT).setText("Record format ");
	    formatCombo = new Combo(this, SWT.DROP_DOWN);
	    formatCombo.setItems(collectionToArray(formats.toArray()));
	    formatCombo.select(0);
	    i = 0;
	    for (Format f: formats) {
	    	if (f.equals(tdh.getRecordingFormat())) {
	    		formatCombo.select(i);
	    		break;
	    	}
	    	i++;
	    }
	    correctRecordFileExtension();
	    
	    formatCombo.addSelectionListener(new SelectionListener(){
	    	@Override
	    	public void widgetSelected(SelectionEvent arg0) {
	    		correctRecordFileExtension();
	    	}
	    	@Override
	    	public void widgetDefaultSelected(SelectionEvent arg0) {
	    		correctRecordFileExtension();	    		
	    	}
	    });
		
		final Button button = new Button(this, SWT.NONE);
		button.setText((tdh.isRecordingInProgress()) ? "STOP" : "REC");
		
		GridData videoGridData = new GridData(GridData.FILL_BOTH);
		vc = new VideoComponent(this, SWT.NONE);
		vc.setLayoutData(videoGridData);
		vc.setSize(320, 240);
		vc.setAspectRatio(4, 3);
		vc.setBackground(new Color(getDisplay(), 0, 0, 0));
		try {
			vcEl = vc.getElement();
			tdh.setVideoSink(vcEl);
		} catch (GstreamerException e) {
			// TODO: Notify the user about the error
			log.warning("A Gstreamer exception is catched: " + e.getMessage());
		}

		button.addListener(SWT.Selection, new Listener(){
			@Override
			public void handleEvent(Event arg0) {
				try {
					if (tdh.isRecordingInProgress()) {
						tdh.stopRecording();
						button.setText("REC");
					} else {
						button.setText("STOP");
						Format format = formats.get(formatCombo.getSelectionIndex());
						Resolution resolution = resolutions.get(resCombo.getSelectionIndex());
						tdh.startRecording(format, resolution, fileText.getText());
					}
				} catch(GstreamerException e) {
					log.warning("Exception caught while Recording: "+e.getMessage());
					e.printStackTrace();
				}
			}
		});
	}
	
	private String[] collectionToArray(Object[] c) {
		String[] ret = new String[c.length];
		int i = 0;
		for (Object o: c) {
			ret[i++] = o.toString();
		}
		
		return ret;	
	}
	
	private void correctRecordFileExtension() {
		Format format = formats.get(formatCombo.getSelectionIndex());
		String filePath = recordFile.getAbsolutePath();
		try {
			filePath = filePath.substring(0, filePath.lastIndexOf('.')+1)+format.getContainer().getExtension();
		} catch (IndexOutOfBoundsException e) {
			filePath += "." + format.getContainer().getExtension();
		}
		recordFile = new File(filePath);
	    fileText.setText(recordFile.getAbsolutePath());
	}
	
	@Override
	public void dispose() {
		try {
			tdh.unsetVideoSink(vcEl);
		} catch (GstreamerException e) {
			// TODO: Notify the user about the error
			log.warning("A Gstreamer exception is catched: " + e.getMessage());
		}
		super.dispose();
	}
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(GUIRecordComposite.class
			.getName());

}
