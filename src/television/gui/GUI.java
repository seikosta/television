package television.gui;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.gstreamer.swt.overlay.VideoComponent;

import television.TVDataHandler;
import television.TVDataHandler.GstreamerException;

public class GUI {
	private TVDataHandler tdh;

	private int lastMenuItem;
	private Composite menuRow;
	private Button[] menuButtons;
	private final static String[] menuButtonNames = { "TV", "Settings",
			"Record", "RTSP" };
	private Composite menuComposite = null;
	private final Display display;
	private final Shell shell;
	private final GUI gui;

	public GUI(TVDataHandler tdh) {
		this.tdh = tdh;
		menuButtons = new Button[menuButtonNames.length];
		
		display = new Display();
		shell = new Shell(display);

		gui = this;
	}

	public void displayGUI() {
		shell.setText("television");
		shell.setLayout(new GridLayout(1, false));

		packMenu();
		menuComposite = new GUITVComposite(shell, SWT.BORDER | SWT.FILL, tdh, this);

		shell.layout(true);
		shell.pack();
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		tdh.destructMainPipeline();
		display.dispose();
	}

	//This listener runs on every menu button press, it changes the buttons' state, and 
	//changer Composites.
	private Listener menuButtonListener = new Listener() {
		public void handleEvent(Event event) {
			for (int i = 0; i < menuButtons.length; ++i) {
				if (event.widget == menuButtons[i]) {
					menuButtons[i].setSelection(true);
					if (lastMenuItem != i) {
						if (menuComposite != null) menuComposite.dispose();
						switch(i) {
							case 0: menuComposite = new GUITVComposite(shell, SWT.BORDER | SWT.FILL, tdh, gui); break;
							case 1: menuComposite = new GUISettingsComposite(shell, SWT.BORDER | SWT.FILL, tdh); break;
							case 2: menuComposite = new GUIRecordComposite(shell, SWT.BORDER | SWT.FILL, tdh, shell); break;
							case 3: menuComposite = new GUIRTSPComposite(shell, SWT.BORDER | SWT.FILL, tdh); break;
							default: break;
						}
						menuComposite.layout(true);
						shell.pack();
						shell.layout(true);
						menuButtons[lastMenuItem].setSelection(false);
					}
					lastMenuItem = i;
				}
			}
		}
	};

	
	private void packMenu() {
		
		menuRow = new Composite(shell, SWT.FILL);
		
		menuRow.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_CENTER));
		RowLayout menuLayout = new RowLayout(SWT.HORIZONTAL);
		menuLayout.center = true;
		menuLayout.fill = true;
		menuLayout.justify = true;
		menuLayout.wrap = false;
		menuLayout.center = true;
		menuRow.setLayout(menuLayout);
		for (int i = 0; i < menuButtonNames.length; ++i) {
			menuButtons[i] = new Button(menuRow, SWT.TOGGLE);
			menuButtons[i].setText(menuButtonNames[i]);
			menuButtons[i].addListener(SWT.Selection, menuButtonListener);
		}
		lastMenuItem = 0;
		menuButtons[lastMenuItem].setSelection(true);		
	}
	
	public void fullscreen(boolean fullScreen) {
		if (fullScreen) {
			if (menuRow != null) menuRow.dispose();
			if (menuComposite != null) menuComposite.dispose();
			
			GridData gd = new GridData(GridData.FILL_BOTH);
			gd.widthHint = 640;
			gd.heightHint = 480;
			gd.grabExcessHorizontalSpace = true;
			gd.grabExcessVerticalSpace = true;	
			
			final VideoComponent vc = new VideoComponent(shell, SWT.NONE);
			vc.setLayoutData(gd);
			vc.setSize(640, 480);
			vc.setAspectRatio(4, 3);
			vc.setBackground(new Color(display, 0, 0, 0));

			vc.addListener(SWT.MouseDoubleClick, new Listener() {
				@Override
				public void handleEvent(Event arg0) {
					try {
						tdh.unsetVideoSink(vc.getElement());
					} catch (GstreamerException e) {
						//TODO: Notify the user about the error
						log.warning("A Gstreamer exception is catched: "+e.getMessage());
					}

					gui.fullscreen(false);
				};
			});
			
			menuComposite = vc;
			
			try { 
				tdh.setVideoSink(vc.getElement());
			} catch (GstreamerException e) {
				//TODO: Notify the user about the error
				log.warning("A Gstreamer exception is catched: "+e.getMessage());
			}

		} else {
			packMenu();
			if (menuComposite != null) menuComposite.dispose();
			switch(lastMenuItem) {
				case 0: menuComposite = new GUITVComposite(shell, SWT.BORDER | SWT.FILL, tdh, this); break;
				case 1: menuComposite = new GUISettingsComposite(shell, SWT.BORDER | SWT.FILL, tdh); break;
				case 2: menuComposite = new GUIRecordComposite(shell, SWT.BORDER | SWT.FILL, tdh, shell); break;
				case 3: menuComposite = new GUIRTSPComposite(shell, SWT.BORDER | SWT.FILL, tdh); break;
			default: break;
			};
		}
		shell.pack();
	}
	
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(GUI.class.getName());

	
}
