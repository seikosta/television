package television.gui;

import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.gstreamer.swt.overlay.VideoComponent;

import television.Configuration;
import television.TVDataHandler;
import television.TVDataHandler.CannotChangeFrequencyException;
import television.TVDataHandler.GstreamerException;
import television.channel.Channel;

public class GUITVComposite extends Composite {
	private TVDataHandler tdh = null;
	private VideoComponent vc = null;
	
	public GUITVComposite(Composite parent, int style, final TVDataHandler tdh, final GUI gui) {
		super(parent, style);
		this.tdh = tdh;
		Configuration conf = Configuration.getInstance();

		this.setLayout(new GridLayout(2, false));
		this.layout(true);
		GridData gdThis = new GridData(GridData.FILL_BOTH);
		this.setLayoutData(gdThis);
		
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.widthHint = 640;
		gd.heightHint = 480;
		vc = new VideoComponent(this, SWT.NONE);
		vc.setLayoutData(gd);
		vc.setAspectRatio(4, 3);
		vc.setBackground(new Color(getDisplay(), 0, 0, 0));
		
		GridData gd2 = new GridData(GridData.VERTICAL_ALIGN_FILL);
		gd2.widthHint = 150;
		//gd2.grabExcessVerticalSpace = true;
		final List list = new List(this, SWT.BORDER | SWT.SINGLE | SWT.V_SCROLL);
		list.setLayoutData(gd2);
		
		int chanSelected = -1;
		Channel tunedChannel = tdh.getTunedChannel();
		for (int i=0; i<Channel.values().length; ++i) {
			String channel = conf.getProperty("Channel_"+i);
			if (channel == null) 
				break;
			if (tunedChannel != null && tunedChannel.equals(Channel.valueOf(channel)))
				chanSelected = i;
			list.add(channel);
		}
		if (chanSelected != -1) {
			list.select(chanSelected);
		}
		
		list.addListener(SWT.Selection, new Listener(){
			@Override
			public void handleEvent(Event event) {
				Channel chan = Channel.valueOf(list.getSelection()[0]);
				try {
					Configuration.getInstance().put("DefaultSelection", chan.name());
					log.info("Changed to " + chan.name() + ";" + "frequency: " + chan.getFreqMHz()+ "MHz" );
					tdh.tuneChannel(chan);
				} catch (CannotChangeFrequencyException ex) {
					//TODO: handle it
				}
			}
		});
		
		vc.addListener(SWT.MouseDoubleClick, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				gui.fullscreen(true);
			};
		});

		try { 
			tdh.setVideoSink(vc.getElement());
		} catch (GstreamerException e) {
			//TODO: Notify the user about the error
			log.warning("A Gstreamer exception is catched: "+e.getMessage());
		}
	}
	
	@Override
	public void dispose() {
		try {
			tdh.unsetVideoSink(vc.getElement());
		} catch (GstreamerException e) {
			//TODO: Notify the user about the error
			log.warning("A Gstreamer exception is catched: "+e.getMessage());
		}
		super.dispose();
	}
	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(GUITVComposite.class.getName());

}
