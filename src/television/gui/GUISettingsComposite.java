package television.gui;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.gstreamer.Element;
import org.gstreamer.swt.overlay.VideoComponent;

import television.Configuration;
import television.TVDataHandler;
import television.TVDataHandler.GstreamerException;
import television.channel.ChannelDetect;
import television.device.AudioDevice;
import television.device.DeviceDiscover;
import television.device.VideoDevice;

public class GUISettingsComposite extends Composite {
	private TVDataHandler tdh = null;
	private VideoComponent vc = null;
	private Composite autodetectComposite = null;
	private Element vcEl = null;
	private ChannelDetect chanDetect;

	public GUISettingsComposite(Composite parent, int style, final TVDataHandler tdh) {
		super(parent, style);
		this.tdh = tdh;
		this.setLayout(new GridLayout(2, false));
		this.layout(true);
		GridData gdThis = new GridData(GridData.FILL_BOTH);
		gdThis.widthHint = 640;
		gdThis.heightHint = 480;
		gdThis.grabExcessHorizontalSpace = true;
		gdThis.grabExcessVerticalSpace = true;
		this.setLayoutData(gdThis);
		chanDetect = new ChannelDetect(tdh, getDisplay());

		new Label(this, SWT.RIGHT_TO_LEFT).setText("Input audio device ");
		final Combo inputAudioCombo = new Combo(this, SWT.DROP_DOWN);
		new Label(this, SWT.RIGHT_TO_LEFT).setText("Output audio device ");
		final Combo outputAudioCombo = new Combo(this, SWT.DROP_DOWN);

		Configuration conf = Configuration.getInstance();
		ArrayList<AudioDevice> al = DeviceDiscover.getAudioDevices();
		int sel1=-1, sel2=-1, sel3=-1;
		for (int i = 0; i < al.size(); ++i) {
			if (al.get(i).toString().equals(conf.getProperty("AudioInputDevice"))) {
				sel1 = i;
			}
			if (al.get(i).toString().equals(conf.getProperty("AudioOutputDevice"))) {
				sel2 = i;
			}
			inputAudioCombo.add(al.get(i).toString());
			outputAudioCombo.add(al.get(i).toString());
		}
		if (sel1 != -1) inputAudioCombo.select(sel1);
		if (sel2 != -2) outputAudioCombo.select(sel2);
		

		new Label(this, SWT.RIGHT_TO_LEFT).setText("Input video device ");
		final Combo inputVideoCombo = new Combo(this, SWT.DROP_DOWN);

		ArrayList<VideoDevice> vl = DeviceDiscover.getVideoDevices();
		for (int i = 0; i < vl.size(); ++i) {
			if (vl.get(i).toString().equals(conf.getProperty("VideoInputDevice"))) {
				sel3 = i;
			}
			inputVideoCombo.add(vl.get(i).toString());
		}
		if (sel3 != -1) inputVideoCombo.select(sel3);

		new Label(this, SWT.RIGHT_TO_LEFT).setText("Signal standard ");
		final Combo signalStandardCombo = new Combo(this, SWT.DROP_DOWN);
		signalStandardCombo.add("PAL");
		signalStandardCombo.add("NTSC");
		String str = conf.getProperty("VideoSignalStandard");
		if (str == null) {
			conf.setProperty("VideoSignalStandard", "PAL");
			signalStandardCombo.select(0);
		} else {
			if (str.equals("PAL")) {
				signalStandardCombo.select(0);
			} else if (str.equals("NTSC")) {
				signalStandardCombo.select(1);
			}
		}
		
		// ----------- Selection listeners --------//
		SelectionListener comboListener = new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
				try {
					AudioDevice audioInputDevice;
					AudioDevice audioOutputDevice;
					VideoDevice videoInputDevice;
					Configuration conf = Configuration.getInstance();
					if (e.widget == inputAudioCombo) {
						audioInputDevice = DeviceDiscover.getAudioDevices().get(inputAudioCombo.getSelectionIndex());
						conf.setProperty("AudioInputDevice", audioInputDevice.toString());
					} else if (e.widget == outputAudioCombo) {
						audioOutputDevice = DeviceDiscover.getAudioDevices().get(outputAudioCombo.getSelectionIndex());
						conf.setProperty("AudioOutputDevice", audioOutputDevice.toString());
					} else if (e.widget == inputVideoCombo) {
						videoInputDevice = DeviceDiscover.getVideoDevices().get(inputVideoCombo.getSelectionIndex());
						conf.setProperty("VideoInputDevice", videoInputDevice.toString());
					}
					tdh.unsetVideoSink(vcEl);
					tdh.destructMainPipeline();
					tdh.constructMainPipeline(
							DeviceDiscover.searchAudioDeviceForString(conf.getProperty("AudioInputDevice")),
							DeviceDiscover.searchAudioDeviceForString(conf.getProperty("AudioOutputDevice")),
							DeviceDiscover.searchVideoDeviceForString(conf.getProperty("VideoInputDevice"))					
					);
					tdh.setVideoSink(vcEl);					
				} catch (GstreamerException e1) {
					log.warning("Gstreamer Exception catched: "+e1.getMessage());
					try {
						tdh.destructMainPipeline();
						tdh.constructMainPipeline(null, null, null);
						tdh.setVideoSink(vcEl);
					} catch (Exception e2) {
						log.warning("Gstreamer exception, with null,null,null pipeline construction: "+e2.getMessage());
						e2.printStackTrace();
					}					
				}
			}
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};
		
		SelectionListener comboVideoStandardListener = new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {
					if (e.widget == signalStandardCombo) {
						Configuration.getInstance().setProperty("VideoSignalStandard", signalStandardCombo.getText());
					}
			}
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		};

		inputAudioCombo.addSelectionListener(comboListener);
		outputAudioCombo.addSelectionListener(comboListener);
		inputVideoCombo.addSelectionListener(comboListener);
		signalStandardCombo.addSelectionListener(comboVideoStandardListener);

		// ------------ Auto detection --------------//
		new Label(this, SWT.RIGHT_TO_LEFT).setText("Video preview ");
		autodetectComposite = new Composite(this, SWT.FILL);
		autodetectComposite.setLayout(new GridLayout(1, false));
		
		GridData videoGridData = new GridData(GridData.FILL_BOTH);
		gdThis.widthHint = 600;
		gdThis.heightHint = 480;
		gdThis.grabExcessHorizontalSpace = true;
		gdThis.grabExcessVerticalSpace = true;

		autodetectComposite.setLayoutData(videoGridData);
		autodetectComposite.layout(true);

		final Button autodetectButton = new Button(autodetectComposite, SWT.NONE);
		ProgressBar autodetectProgressBar = new ProgressBar(autodetectComposite, SWT.NONE);
		chanDetect.setButton(autodetectButton);
		chanDetect.setProgress(autodetectProgressBar);

		
		vc = new VideoComponent(autodetectComposite, SWT.NONE);
		vc.setLayoutData(videoGridData);
		vc.setSize(320, 240);
		vc.setAspectRatio(4, 3);
		vc.setBackground(new Color(getDisplay(), 0, 0, 0));

		try {
			vcEl = vc.getElement();
			tdh.setVideoSink(vcEl);
		} catch (GstreamerException e) {
			// TODO: Notify the user about the error
			log.warning("A Gstreamer exception is catched: " + e.getMessage());
		}
	}

	@Override
	public void dispose() {
		try {
			chanDetect.stopDetect();
			tdh.unsetVideoSink(vcEl);
		} catch (GstreamerException e) {
			// TODO: Notify the user about the error
			log.warning("A Gstreamer exception is catched: " + e.getMessage());
		}
		super.dispose();
	}

	/**
	 * Logger
	 */
	private static Logger log = Logger.getLogger(GUISettingsComposite.class
			.getName());
}
