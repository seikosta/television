package television.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import television.TVDataHandler;
import television.rtsp.RTSPServer;

public class GUIRTSPComposite extends Composite{
	private static final String[] buttonTexts = {"start RTSP Server", "stop RTSP Server"};
	
	public GUIRTSPComposite(Composite parent, int style, final TVDataHandler tdh) {
		super(parent, style);
		this.setLayout(new GridLayout(2, false));
		this.layout(true);
		GridData gdThis = new GridData(GridData.FILL_BOTH);
		gdThis.widthHint = 640;
		gdThis.heightHint = 480;
		gdThis.grabExcessHorizontalSpace = true;
		gdThis.grabExcessVerticalSpace = true;
		this.setLayoutData(gdThis);

		new Label(this, SWT.RIGHT_TO_LEFT).setText("Port ");
		final Text portText = new Text(this, SWT.BORDER);
		if (tdh.getRtspServer() != null)
			portText.setText(""+tdh.getRtspServer().getPort());
		else
			portText.setText(""+RTSPServer.DEFAULT_PORT);
		new Label(this, SWT.RIGHT_TO_LEFT).setText(" ");
		
		final Button rtspButton = new Button(this, SWT.NONE);
		rtspButton.setText((tdh.getRtspServer() == null) ? buttonTexts[0] : buttonTexts[1]);
		rtspButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				if (tdh.getRtspServer() == null) {
					int port = 0;
					try {
						port = Integer.parseInt(portText.getText());
					} catch (Exception e) {}
					tdh.setRtspServer(new RTSPServer(port, tdh));
					portText.setText(""+tdh.getRtspServer().getPort());
					tdh.getRtspServer().start();
					rtspButton.setText(buttonTexts[1]);
				} else {
					tdh.getRtspServer().interrupt();
					tdh.setRtspServer(null);
					rtspButton.setText(buttonTexts[0]);
				}
				
			}
		});

	}
	
}
