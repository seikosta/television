CC=gcc


GST_LIBS=`pkg-config --libs gstreamer-0.10`
GST_CFLAGS=`pkg-config --cflags gstreamer-0.10`

CFLAGS=-g

BINS=television

all: $(BINS)

television: television.c
	$(CC) $(CFLAGS) $(GST_LIBS) $(GST_CFLAGS) $+ -o $@

clean:
	rm -rf $(BINS)
