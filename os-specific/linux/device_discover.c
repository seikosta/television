#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include <fcntl.h>

#ifndef LIBRARY
#include <stdio.h>
#endif

int v4l_discover(char* device) {
	int fd, version;
	struct v4l2_capability cap;

	fd = open(device, O_RDWR);
	version = (ioctl(fd, VIDIOC_QUERYCAP, &cap) == 0) ? 2 : 1;
	
	close(fd);
	return version;
}

#ifndef LIBRARY
int main(int argc, char** argv) {
	if (argc != 2) {
		fprintf(stderr, "Usgae:\n\tdevice_discover /dev/videoN\n");
		return 1;
	}
	if (v4l_discover(argv[1])) {
		printf("The %s is a V4L2 device\n", argv[1]);
	} else {
		printf("The %s is a V4L1 device\n", argv[1]);
	}
	return 0;
}
#endif
