#include <gst/gst.h>
#include <gst/interfaces/tuner.h>

/*
 * Global objects are usually a bad thing. For the purpose of this
 * example, we will use them, however.
 */
GstElement *pipeline, *videosource, *ffmpegcolorspace, *videosink, *audiosource, *audiosink, *audioresample, *audiobin, *videobin, *audiotee, *videotee, *muxer, *filesink, *savebin;

/*
static gboolean next_video() {

	gst_element_set_state (pipeline, GST_STATE_READY);
	g_object_set(G_OBJECT(source), "location", "o2.mkv", NULL);
	gst_element_set_state (pipeline, GST_STATE_PLAYING);


	return TRUE;
}
*/

static gboolean bus_call (GstBus *bus, GstMessage *msg, gpointer data)
{
	GMainLoop *loop = (GMainLoop *) data;
	switch (GST_MESSAGE_TYPE (msg)) {
		case GST_MESSAGE_EOS:
			g_print ("End-of-stream\n");
			g_main_loop_quit (loop);
			break;
			case GST_MESSAGE_ERROR: {
				gchar *debug;
				GError *err;
				gst_message_parse_error (msg, &err, &debug);
				g_free (debug);
				g_print ("Error: %s\n", err->message);
				g_error_free (err);
				g_main_loop_quit (loop);
				break;
			}
		default:
			break;
	}
	return TRUE;
}


int main (int    argc, char *argv[])
{
	GstCaps *filter;

	//GstTuner *tuner = NULL;
	//gchar *channel = "Composite1";

	GMainLoop *loop;
	GstBus *bus;

	/* initialize GStreamer */
	gst_init (&argc, &argv);
	loop = g_main_loop_new (NULL, FALSE);

	/* create elements */

	pipeline = gst_pipeline_new ("TV_pipeline");
	videosource = gst_element_factory_make ("v4l2src", "vsource");
	videosink = gst_element_factory_make ("xvimagesink", "vsink");
	ffmpegcolorspace = gst_element_factory_make("ffmpegcolorspace", "ffmpegcsp");
	audiosource = gst_element_factory_make ("alsasrc", "asource");
	audiosink = gst_element_factory_make ("alsasink", "asink");
	audioresample = gst_element_factory_make ("audioresample", "aresample");
	audiotee = gst_element_factory_make ("tee", "at");
	videotee = gst_element_factory_make ("tee", "vt");
	muxer = gst_element_factory_make ("avimuxer", "mux");
	filesink = gst_element_factory_make ("filesink", "fsink");

	audiobin = gst_bin_new ("abin");
	videobin = gst_bin_new ("vbin");
	savebin = gst_bin_new ("sbin");

	if (!pipeline || !videosource || !videosink || !audiosource || !audiosink || !audioresample || !audiobin) {
		g_print (" --- ERROR One element could not be created\n");
		return -1;
	}
	//Setting up source properties
	g_object_set(G_OBJECT(videosource), "device", "/dev/video2", NULL);
	g_object_set(G_OBJECT(audiosource), "device", "hw:1,0", NULL);
	g_object_set(G_OBJECT(filesink), "location", "save.avi", NULL);

	g_object_set(G_OBJECT(videobin), "async-handling", TRUE, NULL);
	g_object_set(G_OBJECT(audiobin), "async-handling", TRUE, NULL);
	g_object_set(G_OBJECT(savebin), "async-handling", TRUE, NULL);
	

	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	gst_bus_add_watch (bus, bus_call, loop);
	gst_object_unref (bus);

	/* put all elements in a bin */
	gst_bin_add_many (GST_BIN (audiobin), audiosource, audioresample, audiotee, audiosink, NULL);
	gst_bin_add_many (GST_BIN (videobin), videosource,  ffmpegcolorspace, videotee, videosink, NULL);
	gst_bin_add_many (GST_BIN (savebin), muxer, filesink, NULL);
	gst_bin_add_many (GST_BIN (pipeline), audiobin, videobin, NULL);
	


	if (!gst_element_link_many(videosource, ffmpegcolorspace, videotee, videosink, NULL)) {
		printf(" --- ERROR in linking 0\n");
		return -1;
	}

	filter = gst_caps_new_simple("audio/x-raw-int", "rate", G_TYPE_INT, 32000, NULL);
	if (!gst_element_link_filtered(audiosource, audioresample, filter)) {
		printf(" --- ERROR in linking 2\n");
		return -1;
	}

	if (!gst_element_link_many(audioresample, audiotee, audiosink, NULL)) {
		printf(" --- ERROR in linking 3\n");
		return -1;
	}
/*
	if (!gst_element_link(videotee, muxer)) {
		printf(" --- ERROR in linking [4]\n");
		return -1;
	}
	if (!gst_element_link(audiotee, muxer)) {
		printf(" --- ERROR in linking [5]\n");
		return -1;
	}
	if (!gst_element_link(muxer, filesink)) {
		printf(" --- ERROR in linking [6]\n");
		return -1;
	}
*/

	/* Now set to playing and iterate. */
	g_print ("Setting to PLAYING\n");
	gst_element_set_state (pipeline, GST_STATE_PLAYING);
	g_print ("Running\n\n");


 	g_main_loop_run (loop);

	g_print ("Returned, stopping playback\n");
	gst_element_set_state (pipeline, GST_STATE_NULL);
	g_print ("Deleting pipeline\n");
	gst_object_unref (GST_OBJECT (pipeline));

	return 0;
}
